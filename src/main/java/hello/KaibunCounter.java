package hello;

import org.springframework.stereotype.Component;

//@Component
public class KaibunCounter {
  
  private final String origin;
  private final int length;
  private int count = 0;
  
  public KaibunCounter(String origin) {
    this.origin = origin;
    this.length = this.origin.length();
  }

  public int count() {
    System.out.println("INPUT: " + origin);
    System.out.println("length: " + length);
    
    for(int i=0;i<this.length;i++) {
      StringBuilder addedString = addCharFromTop(i);
      if(kaibunHantei(addedString)) {
        count = i;
        break;
      }
      
    }
    return length+count;
  }
  
  public StringBuilder addCharFromTop(int charlen) {
    
      StringBuilder sb = new StringBuilder();
      sb.append(origin);
      StringBuilder charToAdd = new StringBuilder(origin.substring(0,charlen)).reverse();
      sb.append(charToAdd.toString());
      
      return sb;
  }
  
  private boolean kaibunHantei(StringBuilder input) {
    int inputLength = input.length();
    
    System.out.println("kaibunHantei: " + input.toString());
    System.out.println("inputLength: " + inputLength);
    
    for(int i=0;i < inputLength/2;i++){
      System.out.println(input.charAt(i));
      if(input.charAt(i) != input.charAt(inputLength-i-1)) {
          return false;
      }
    }
    System.out.println("true?");
    
    return true;
  }

}