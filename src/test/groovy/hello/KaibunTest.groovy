package hello

import static java.lang.System.out
import static org.hamcrest.CoreMatchers.is
import static org.junit.Assert.assertThat

import hello.AppConfigAutoScan

import org.junit.runner.RunWith
import org.springframework.test.context.ContextConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.junit4.SpringRunner
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

import spock.lang.Specification
import spock.lang.Unroll

@ContextConfiguration
//@RunWith(SpringRunner.class)
// @RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class KaibunTest extends Specification {

//  @Autowired
//  private KaibunCounter kaibunCounter

  @Unroll
  def "#input を廻文にするには #count 文字必要"() {
    setup:
      KaibunCounter kaibunCounter = new KaibunCounter(input) 

    expect:
    
      kaibunCounter.count() == count

    where:
    input||count
    'abab'||5
    'abacaba'||7
    'abb'||4
    'a'||1
    ''||0
    'qwerty'||11
    'abdfhdyrbdbsdfghjkllkjhgfds'||38
    
      // true
// 'abab' -> 'a'を足して 5
// 'abacaba' -> もう回文なので 7
// 'qwerty' -> 'trewq'を足して 11
// 'abcdedc' -> 'ba'を足して 9
// 'abcda' -> 'dcba'を足して 9
// 'abdfhdyrbdbsdfghjkllkjhgfds' -> 'kllk'の部分で折り返しているので、いっぱいそれ以降を足して 38
// 'a' -> もう回文なので 1
// '' -> もう回文なので 0      
      
  }
}